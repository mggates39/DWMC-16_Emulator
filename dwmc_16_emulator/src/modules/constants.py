'''Constants used in the DWMC-16 emulator'''

from enum import Enum


class CadrId(str, Enum):
    '''Class containing Constants for the CADR Lines of the Control Bus'''

    # Registers
    R00 = 0x0
    R01 = 0x1
    R02 = 0x2
    R03 = 0x3
    R04 = 0x4
    R05 = 0x5
    R06 = 0x6
    R07 = 0x7
    R08 = 0x8
    R09 = 0x9
    R10 = 0xA
    R11 = 0xB
    R12 = 0xC
    R13 = 0xD
    R14 = 0xE
    R15 = 0xF
    BT = 0x8  # Bit Test Register
    F = 0x9  # Flag Register
    PCL = 0xA  # Program Counter Low Register
    PCH = 0xB  # Program Counter High Register
    SPL = 0xC  # Stack Pointer Low Register
    SPH = 0xD  # Stack Pointer High Register
    ZL = 0xE  # Z Index Low Register
    ZH = 0xF  # Z Index High Register

    # Flags
    Z = 0x0  # Zero Flag
    C = 0x2  # Carry Flag
    QC = 0x3  # Quarter Carry Flag
    HC = 0x4  # Half Carry Flag
    TC = 0x5  # Three Quaters Carry Flag
    EQ = 0x7  # Equality Flag
    N = 0x8  # Negative Flag
    OF = 0x9  # Overflow Flag
    IO = 0xA  # IO Memory Access Flag
    IE = 0xC  # Interrupt Enable Flag
    I0 = 0xD  # Interrupt 0 Flag
    I1 = 0xE  # Interrupt 1 Flag
    I3 = 0xF  # Interrupt 2 Flag

    # Bit Test
    B00 = 0x0
    B01 = 0x1
    B02 = 0x2
    B03 = 0x3
    B04 = 0x4
    B05 = 0x5
    B06 = 0x6
    B07 = 0x7
    B08 = 0x8
    B09 = 0x9
    B10 = 0xA
    B11 = 0xB
    B12 = 0xC
    B13 = 0xD
    B14 = 0xE
    B15 = 0xF

    # Interrupt Vectors
    RV = 0x0  # Reset Vector
    INT1 = 0x1  # Interrupt Vector 1
    INT2 = 0x2  # Interrupt Vector 2
    INT3 = 0x3  # Interrupt Vector 3
    INT4 = 0x4  # Interrupt Vector 4
    INT5 = 0x5  # Interrupt Vector 5
    INT6 = 0x6  # Interrupt Vector 6
    INT7 = 0x7  # Interrupt Vector 7
